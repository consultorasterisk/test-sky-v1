const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const Usuario = require('../models/usuario');

exports.signup = (req, res) => {
       
    const _email = req.body.email;
    jwt.sign({ _email }, 'skybrasil', { expiresIn: '1800s' }, (err, token) => {        
        Usuario.find({ email: req.body.email })
        .then(result => {
            if ( result.length > 0 ) {
                res.status(302).json({
                    message: "E-mail já existente" 
                });
            }else{                
                const usuario = new Usuario({
                    _id: mongoose.Types.ObjectId(),
                    nome: req.body.nome,
                    email: req.body.email,
                    senha: req.body.senha,
                    telefones: req.body.telefones,
                    data_criacao: Date.now(),
                    data_atualizacao: Date.now(),
                    ultimo_login: Date.now(),                    
                });
                
                usuario.save()
                .then(result => {                                        
                    res.status(200).json({ usuario: result, token: token });
                }).catch(err => console.log(err));
            }
        });    
    });
} 

exports.signin = (req, res) => {

    const _email = req.body.email;
    const _senha = req.body.senha;
    Usuario.find({ email: req.body.email })
    .then(result => {
        if (result.length == 0) {
            res.status(401).json({
                message: "Usuário e/ou senha inválidos"
            });   
        }else{            
            if ( _senha != result[0]['senha'] ) {
                res.status(401).json({
                    message: "Usuário e/ou senha inválidos"
                });
            }else{    
                jwt.sign({ userid: result[0]._id, email: _email }, 'skybrasil', { expiresIn: '1800s' }, (err, token) => {
                    res.status(200).json({                        
                        usuario: result,
                        token: token
                    });
                });                
            }   
        }    
    }).catch(err => {
        res.status(500).json({
            message: err.message
        });            
    });       
}

exports.buscarusuario = (req, res) => {
    jwt.verify(req.token, 'skybrasil', (err, authData) => {        
        if ( err ) {            
            if ( err.message == "jwt expired" ) {
                res.status(403).json({
                    message: 'Sessão inválida'
                });
            }else{
                res.status(403).json({
                    message: 'Não autorizado'
                });
            }
        }else{            
            Usuario.find({ _id: authData.userid })
            .then(result => {
                res.status(200).json(result);
            }).catch(err => {
                res.status(500).json({
                    message: err.message
                });
            });
        }
    });    
}