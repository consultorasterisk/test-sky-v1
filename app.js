const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();

const signRoute = require('./routes/sign')

// connect mongodb atlas
const uri = "mongodb+srv://testsky:skybrasil@cluster0-4pjqp.mongodb.net/test?retryWrites=true&w=majority";
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });

// midleware
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());

// routers
app.use('/',  signRoute);

// errors
app.use((req, res, next) => {
    const error = new Error('erro');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
    next();
});

module.exports = app;