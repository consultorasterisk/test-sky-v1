const http = require('http');
const app = require('./app');
const port = process.env.PORT = 3000;
const server = http.createServer(app);

server.listen(port, function () {
    console.log('Server - Listening at Port ' + port);
});

process.on('uncaughtException', function (err) {
    console.error(' ');
    console.error('----- ' + (new Date).toUTCString() + ' ----------------------------------')
    console.error('Erro uncaughtException: ', err.message)
    console.error(err.stack)
    console.error(' ');
    return
});