const request = require('supertest');
const app = require('../app');

describe('API', function() {    

    let token = null;
    let objsignup = { "nome": "EDSON BARBOSA DA SILVA", "email": "edsondev@hotmail.com", "senha": "123456", "telefones": [{ "numero": "951010221", "ddd": "11" }] };
    let objsignin = { "email": objsignup.email, "senha": objsignup.senha };

    it('Testing API: Sign Up...', function(done) {                      
        request(app)
          .post("/signup")          
          .send(objsignup)
          .expect(200, done)          
    });

    it('Testing API: Sign In...', function(done) {
      request(app)
        .post("/signin")          
        .send(objsignin)        
        .end(function(err, res) {
          token = res.body.token;        
          done();
        })                        
    });    
   
    it('Testing API: Buscar Usuário...', function(done) {        
        request(app)
        .post("/buscarusuario")          
        .set('Authorization', "Bearer " + token )
        .expect(200, done)                
    });       
});