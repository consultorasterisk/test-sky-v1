const mongoose = require('mongoose');

const usuarioSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nome: String,
    email: String,
    senha: String,
    telefones: Object,
    data_criacao: Date,
    data_atualizacao: Date,
    ultimo_login: Date
});

module.exports = mongoose.model('Usuario', usuarioSchema);