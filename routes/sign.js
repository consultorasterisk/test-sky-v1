const express = require('express');
const router = express.Router();

const controller = require('../controllers/sign');

router.post('/signup', controller.signup);
router.post('/signin', controller.signin);
router.post('/buscarusuario', vtoken, controller.buscarusuario);

function vtoken(req, res, next) {    
    const bearerHeader = req.headers['authorization'];    
    if(typeof bearerHeader !== 'undefined') {
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      req.token = bearerToken;
      next();
    } else {
      res.status(403).json({
          message: "Não autorizado" 
      });
    }
}

module.exports = router;